from sudoku import Sudoku
import numpy as np
from helper import Helper


class DFS:
    def __init__(self, max_nodes: int=10):
        self.node_count = 0
        self.max_nodes = max_nodes
        self.generated_nodes = 0
        self.help = Helper()

    def run(self, sudoku: Sudoku, choices: int=0):
        print(" ****************************************************************************** ")
        #print("# ================================================================================================= #\n")
        result = self.dfs(sudoku, choices)
        print('\n\tNodes Generated: %d' % self.generated_nodes)
        print('\tExpanded nodes: %d' % self.node_count)

        if not result:
            print("\tNot solved!")
            return False
        else:
            print("\tSOLVED!")
            return True
        print()

    def dfs(self, sudoku: Sudoku, choices: int):
        if self.node_count == self.max_nodes:
            return False
        self.node_count += 1

        # print(self.node_count)

        possibilities = sudoku.generate_possibility()
        moves = self.split_possibilities(possibilities)
        self.generated_nodes += len(moves)
        sudokus = []

        for move in moves:
            new = sudoku.copy()
            input_status = new.input_val(move[0], move[1], move[2])
            new.apply_rules(choices)
            if self.help.solved(new.table):
                self.help.show_grid(new)
                return True

            if input_status:
                sudokus.append(new)
            else:
                del new

        sudokus.sort(key=lambda s: 81 - s.count_not_empty())
        for s in sudokus:
            if self.dfs(s, choices):
                return True

        return False

    def split_possibilities(self, possibilities: list):
        moves = []
        sz = np.array(possibilities).shape

        for i in range(sz[0]):
            for j in range(sz[0]):
                if not possibilities[i][j]:
                    continue

                for val in possibilities[i][j]:
                    moves.append([i, j, val])

        return moves
