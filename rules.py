from helper import Helper
import numpy as np
from copy import deepcopy


def hidden_singles(sudoku):
    h = Helper()
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    rows = [[[[j, i], possibilities[j][i]]
             for i in range(sz[1])] for j in range(sz[0])]
    for row in rows:
        if hidden_single(row, sudoku):
            status = True

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    columns = [[[[i, j], possibilities[i][j]]
                for i in range(sz[0])] for j in range(sz[1])]
    for column in columns:
        if hidden_single(column, sudoku):
            status = True

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    blocks = [
        [[[i, j], possibilities[i][j]] for i in range(sz[0]) for j in range(
            sz[1]) if h.get_block_label(i, j) == block]
        for block in range(1, 10)]
    for block in blocks:
        if hidden_single(block, sudoku):
            status = True

    return status


def hidden_single(group: list, sudoku):
    status = False
    for num in range(1, 10):
        idxs = []
        for cell in group:
            if num in cell[1]:
                idxs.append(cell[0])

        if len(idxs) == 1:
            status = True
            # print(idxs[0], num)
            sudoku.input_val(idxs[0][0], idxs[0][1], num)

    return status


def hidden_pairs(sudoku):
    h = Helper()
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    groups = []
    groups.extend([[[[j, i], possibilities[j][i]]
                    for i in range(sz[1])] for j in range(sz[0])])
    groups.extend([[[[i, j], possibilities[i][j]]
                    for i in range(sz[0])] for j in range(sz[1])])
    groups.extend(
        [[[[i, j], possibilities[i][j]] for i in range(sz[0]) for j in range(sz[1]) if h.get_block_label(i, j) == block]
         for block in range(1, 10)])

    for group in groups:
        if hidden_pair(group, sudoku):
            status = True

    return status


def hidden_pair(group: list, sudoku):
    status = False

    idxs = [[] for _ in range(9)]
    for num in range(1, 10):
        for cell in group:
            if num in cell[1]:
                idxs[num - 1].append(cell[0])

    pairs = []
    for i in range(len(idxs) - 1):
        obs = idxs[i]
        if len(obs) != 2:
            continue

        for j in range(i + 1, len(idxs)):
            if idxs[j] == obs:
                pairs.append([obs, [(i + 1), (j + 1)]])

    # print(pairs)

    for pair in pairs:
        cell1 = sudoku.possibilities[pair[0][0][0]][pair[0][0][1]]
        cell2 = sudoku.possibilities[pair[0][1][0]][pair[0][1][1]]

        cells = [cell1, cell2]
        for cell in cells:
            if len(cell) > 2:
                cell.clear()
                cell.extend([pair[1][0], pair[1][1]])
                status = True

    return status


def hidden_triples(sudoku):
    h = Helper()
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    groups = []
    groups.extend([[[[j, i], possibilities[j][i]]
                    for i in range(sz[1])] for j in range(sz[0])])
    groups.extend([[[[i, j], possibilities[i][j]]
                    for i in range(sz[0])] for j in range(sz[1])])
    groups.extend(
        [[[[i, j], possibilities[i][j]] for i in range(sz[0]) for j in range(sz[1]) if h.get_block_label(i, j) == block]
         for block in range(1, 10)])

    for group in groups:
        if hidden_triple(group, sudoku):
            status = True

    return status


def hidden_triple(group: list, sudoku):
    status = False

    idxs = [[] for _ in range(9)]
    for num in range(1, 10):
        for cell in group:
            if num in cell[1]:
                idxs[num - 1].append(cell[0])

    pairs = []
    for i in range(len(idxs) - 2):
        obs = idxs[i]
        if len(obs) != 3:
            continue

        for j in range(i + 1, len(idxs) - 1):
            if idxs[j] == obs:
                for k in range(j + 1, len(idxs)):
                    if idxs[k] == obs:
                        pairs.append([obs, [(i + 1), (j + 1), (k + 1)]])

    # print(pairs)

    for pair in pairs:
        cell1 = sudoku.possibilities[pair[0][0][0]][pair[0][0][1]]
        cell2 = sudoku.possibilities[pair[0][1][0]][pair[0][1][1]]
        cell3 = sudoku.possibilities[pair[0][2][0]][pair[0][2][1]]

        cells = [cell1, cell2, cell3]
        for cell in cells:
            if len(cell) > 3:
                cell.clear()
                cell.extend([pair[1][0], pair[1][1], pair[1][2]])

                status = True

    return status


def naked_singles(sudoku):
    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape

    for i in range(sz[0]):
        for j in range(sz[1]):
            if possibilities[i][j] and len(possibilities[i][j]) == 1:
                sudoku.input_val(i, j, possibilities[i][j][0])
                return True
    return False


def naked_pairs(sudoku):
    h = Helper()
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    groups = []
    groups.extend([[[[i, j], possibilities[i][j]]
                    for i in range(sz[1])] for j in range(sz[0])])
    groups.extend([[[[i, j], possibilities[i][j]]
                    for i in range(sz[0])] for j in range(sz[1])])
    groups.extend(
        [[[[i, j], possibilities[i][j]] for i in range(sz[0]) for j in range(sz[1]) if h.get_block_label(i, j) == block]
         for block in range(1, 10)])

    for group in groups:
        if naked_pair(group, sudoku):
            status = True

    return status


def naked_pair(group: list, sudoku):
    pairs = []

    for i in range(len(group) - 1):
        if len(group[i][1]) != 2:
            continue

        for j in range(i + 1, len(group)):
            if group[j][1] == group[i][1]:
                pairs.append(deepcopy(group[i][1]))

    if not pairs:
        return False

    status = False
    for pair in pairs:
        for cell in group:
            # print(cell)
            if cell[1] == pair:
                continue

            if pair[0] in cell[1]:
                sudoku.possibilities[cell[0][0]][cell[0][1]].remove(pair[0])
                status = True

            if pair[1] in cell[1]:
                sudoku.possibilities[cell[0][0]][cell[0][1]].remove(pair[1])
                status = True

    return status


def naked_triples(sudoku):
    h = Helper()
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    groups = []
    groups.extend([[[[i, j], possibilities[i][j]]
                    for i in range(sz[1])] for j in range(sz[0])])
    groups.extend([[[[i, j], possibilities[i][j]]
                    for i in range(sz[0])] for j in range(sz[1])])
    groups.extend(
        [[[[i, j], possibilities[i][j]] for i in range(sz[0]) for j in range(sz[1]) if h.get_block_label(i, j) == block]
         for block in range(1, 10)])

    for group in groups:
        if naked_triple(group, sudoku):
            status = True

    return status


def naked_triple(group: list, sudoku):
    pairs = []

    for i in range(len(group) - 2):
        if len(group[i][1]) != 3:
            continue

        for j in range(i + 1, len(group) - 1):
            if group[j][1] == group[i][1]:
                for k in range(j + 1, len(group)):
                    if group[k][1] == group[i][1]:
                        pairs.append(deepcopy(group[i][1]))

    if not pairs:
        return False

    status = False
    for pair in pairs:
        for cell in group:
            if cell[1] == pair:
                continue

            if pair[0] in cell[1]:
                sudoku.possibilities[cell[0][0]][cell[0][1]].remove(pair[0])
                status = True

            if pair[1] in cell[1]:
                sudoku.possibilities[cell[0][0]][cell[0][1]].remove(pair[1])
                status = True

            if pair[2] in cell[1]:
                sudoku.possibilities[cell[0][0]][cell[0][1]].remove(pair[2])
                status = True

    return status


def x_wings(sudoku):
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    rows = [[[[j, i], possibilities[j][i]]
             for i in range(sz[1])] for j in range(sz[0])]
    if x_wing(rows, sudoku):
        status = True

    columns = [[[[i, j], possibilities[i][j]]
                for i in range(sz[0])] for j in range(sz[1])]
    if x_wing(columns, sudoku):
        status = True

    return status


def x_wing(groups: list, sudoku):
    status = False

    groups_idxs = []
    for group in groups:
        idxs = [[] for _ in range(9)]

        for num in range(1, 10):
            for cell in group:
                if num in cell[1]:
                    idxs[num - 1].append(cell[0])

        groups_idxs.append(idxs)

    wings = []

    for i in range(len(groups_idxs) - 1):
        for num in range(9):
            if len(groups_idxs[i][num]) != 2:
                continue

            obs1 = groups_idxs[i][num]
            for j in range(i + 1, len(groups_idxs)):
                if len(groups_idxs[j][num]) != 2:
                    continue

                obs2 = groups_idxs[j][num]
                if (obs1[0][1] == obs2[0][1] and obs1[1][1] == obs2[1][1]) or (
                        obs1[0][0] == obs2[0][0] and obs1[1][0] == obs2[1][0]):
                    wings.append(
                        [[obs1[0], obs1[1], obs2[0], obs2[1]], (num + 1)])

    if not wings:
        return False

    # print(wings)
    sz = np.array(sudoku.possibilities).shape

    for wing in wings:
        modify_status = False

        cells1 = [[wing[0][0][0], j] for j in range(
            sz[1]) if j != wing[0][0][1] and j != wing[0][1][1]]
        cells2 = [[wing[0][3][0], j] for j in range(
            sz[1]) if j != wing[0][3][1] and j != wing[0][2][1]]
        cells3 = [[i, wing[0][0][1]] for i in range(
            sz[0]) if i != wing[0][0][0] and i != wing[0][1][0]]
        cells4 = [[i, wing[0][3][1]] for i in range(
            sz[0]) if i != wing[0][3][0] and i != wing[0][2][0]]

        for cell in [cells1, cells2, cells3, cells4]:
            for c in cell:
                if wing[1] in sudoku.possibilities[c[0]][c[1]]:
                    sudoku.possibilities[c[0]][c[1]].remove(wing[1])
                    status = True
                    modify_status = True

        if modify_status:
            sudoku.input_val(wing[0][0][0], wing[0][0][1], wing[1])
            sudoku.input_val(wing[0][3][0], wing[0][3][1], wing[1])
            if wing[1] in sudoku.possibilities[wing[0][1][0]][wing[0][1][1]]:
                sudoku.possibilities[wing[0][1][0]
                                     ][wing[0][1][1]].remove(wing[1])

            if wing[1] in sudoku.possibilities[wing[0][2][0]][wing[0][2][1]]:
                sudoku.possibilities[wing[0][2][0]
                                     ][wing[0][2][1]].remove(wing[1])

    return status


def swordfishes(sudoku):
    status = False

    possibilities = sudoku.possibilities
    sz = np.array(possibilities).shape
    rows = [[[[j, i], possibilities[j][i]]
             for i in range(sz[1])] for j in range(sz[0])]
    if swordfish(rows, sudoku):
        status = True

    columns = [[[[i, j], possibilities[i][j]]
                for i in range(sz[0])] for j in range(sz[1])]
    if swordfish(columns, sudoku):
        status = True

    return status


def swordfish(groups: list, sudoku):
    status = False

    groups_idxs = []
    for group in groups:
        idxs = [[] for _ in range(9)]

        for num in range(1, 10):
            for cell in group:
                if num in cell[1]:
                    idxs[num - 1].append(cell[0])

        groups_idxs.append(idxs)

    fishes = []

    for i in range(len(groups_idxs) - 2):
        for num in range(9):
            num_idxs = []
            if len(groups_idxs[i][num]) != 2 and len(groups_idxs[i][num]) != 3:
                continue

            num_idxs.extend(groups_idxs[i][num])
            for j in range(i + 1, len(groups_idxs) - 1):
                if len(groups_idxs[j][num]) != 2 and len(groups_idxs[j][num]) != 3:
                    continue

                obs2 = groups_idxs[j][num]
                if any(elem in [ni[0] for ni in num_idxs] for elem in [o[0] for o in obs2]) or any(
                        elem in [ni[1] for ni in num_idxs] for elem in [o[1] for o in obs2]):
                    tmp1 = deepcopy(num_idxs)
                    tmp1.extend(obs2)

                    for k in range(j + 1, len(groups_idxs)):
                        if len(groups_idxs[k][num]) != 2 and len(groups_idxs[k][num]) != 3:
                            continue

                        obs3 = groups_idxs[k][num]
                        if any(elem in [ni[0] for ni in num_idxs] for elem in [o[0] for o in obs3]) or any(
                                elem in [ni[1] for ni in num_idxs] for elem in [o[1] for o in obs3]):
                            tmp2 = deepcopy(tmp1)
                            tmp2.extend(obs3)

                            if len(list(set([n[0] for n in tmp2]))) == 3 and len(list(set([n[1] for n in tmp2]))) == 3:
                                fishes.append([tmp2, (num+1)])

    if not fishes:
        return False

    sz = np.array(sudoku.possibilities).shape

    for fish in fishes:
        # print(fish)
        rows_idx = list(set([f[0] for f in fish[0]]))
        col_idx = list(set([f[1] for f in fish[0]]))

        cells = []
        for idx in rows_idx:
            cells.append([[idx, j]
                          for j in range(sz[1]) if [idx, j] not in fish[0]])

        for idx in col_idx:
            cells.append([[i, idx]
                          for i in range(sz[0]) if [i, idx] not in fish[0]])

        for cell in cells:
            for c in cell:
                if fish[1] in sudoku.possibilities[c[0]][c[1]]:
                    sudoku.possibilities[c[0]][c[1]].remove(fish[1])
                    status = True

    return status
