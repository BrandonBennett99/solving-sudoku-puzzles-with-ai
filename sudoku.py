from helper import Helper
from copy import deepcopy
import rules as r
import numpy as np


class Sudoku:
    def __init__(self, table: list, block_label: list):
        self.table = deepcopy(table)
        self.status = True

        self.block_label = deepcopy(block_label)
        self.help = Helper()
        self.possibilities = self.generate_possibility()

    def copy(self):
        c = Sudoku(self.table, self.block_label)
        return c

    def input_val(self, row, col, num):
        if self.table[row][col] != '':
            return False

        self.table[row][col] = num
        self.possibilities[row][col] = []

        sz = np.array(self.possibilities).shape
        for i in range(sz[0]):
            for j in range(sz[1]):
                if i == row or col == j or self.block_label[row][col] == self.block_label[i][j]:
                    if num in self.possibilities[i][j]:
                        self.possibilities[i][j].remove(num)

        return True
        
    #calls the rules from rules.py file
    def apply_rule(self, rule: str):
        if rule == "hidden singles":
            return r.hidden_singles(self)
        elif rule == "hidden pairs":
            return r.hidden_pairs(self)
        elif rule == "hidden triples":
            return r.hidden_triples(self)
        elif rule == "naked singles":
            return r.naked_singles(self)
        elif rule == "naked pairs":
            return r.naked_pairs(self)
        elif rule == "naked triples":
            return r.naked_triples(self)
        elif rule == "x-wings":
            return r.x_wings(self)
        elif rule == "swordfish":
            return r.swordfishes(self)

    #traces the users choice, checking if the choice chosen trule matches
    #one of the pre-defined rules
    def apply_rules(self, choices: int):
        new_sudoku = False

        if choices == 0:
            rules = ["naked singles"]
        elif choices == 1:
            rules = ["naked singles", "naked pairs"]
        elif choices == 2:
            rules = ["naked singles", "naked pairs", "naked triples"]
        elif choices == 3:
            rules = ["naked singles", "naked pairs",
                          "naked triples", "hidden singles"]
        elif choices == 4:
            rules = ["naked singles", "naked pairs",
                          "naked triples", "hidden singles", "hidden pairs"]
        elif choices == 5:
            rules = ["naked singles", "naked pairs",
                          "naked triples", "hidden singles", "hidden pairs", "hidden triples"]
        elif choices == 6:
            rules = ["naked singles", "naked pairs",
                          "naked triples", "hidden singles", "hidden pairs", "hidden triples", "x-wings"]
        else:
            rules = ["naked singles", "naked pairs",
                          "naked triples", "hidden singles", "hidden pairs", "hidden triples", "x-wings", "swordfish"]

        #loops through and counts how many times a rule was applied.
        count = 0
        while True:
            # print(count)
            loop = False
            for rule in rules:
                if self.apply_rule(rule):
                    loop = True
                    new_sudoku = True

            if not loop:
                break

            count += 1

        return new_sudoku

    def generate_possibility(self):
        return self.help.get_all_possibility(self.table)

    def count_not_empty(self):
        sz = np.array(self.table).shape
        not_empty = [self.table[i][j] for i in range(
            sz[0]) for j in range(sz[0]) if self.table[i][j] != '']

        return len(not_empty)
